import { createContext } from "react";
import { assign, Machine } from "xstate";
import axios, { AxiosError } from "axios";

const STATUS_API = `${process.env.NEXT_PUBLIC_API}/accounts/status/`;
function invokeFetchUserData(context) {
  return axios.get(STATUS_API, {
    headers: {
      Authorization: `Token ${context.token}`,
    },
  });
}

const authMachine = Machine({
  initial: "anon",
  context: {
    token: null,
    user: { isAuthenticated: false },
  },
  states: {
    anon: {
      on: {
        FETCH: {
          target: "loading",
          actions: assign({
            token: (context, event) => event.token,
          }),
        },
      },
    },
    loading: {
      invoke: {
        id: "fetch-userdata",
        src: invokeFetchUserData,
        onDone: {
          target: "success",
          actions: assign({
            user: (_, event) => {
              let user = event.data.data;
              user["isAuthenticated"] = user.is_authenticated;
              delete user["is_authenticated"];
              return user;
            },
          }),
        },
        onError: "failed",
      },
    },
    success: {
      on: {
        LOGOUT: {
          target: "anon",
          actions: assign({
            token: null,
            user: { isAuthenticated: false },
          }),
        },
      },
    },
    failed: {},
  },
});

export default authMachine;
