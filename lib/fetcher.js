import axios from "axios";

function fetcher(...args) {
  return axios.get(...args).then((response) => response.data);
}

export default fetcher;
