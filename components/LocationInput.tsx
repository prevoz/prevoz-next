import React, { FunctionComponent, useEffect, useState } from "react";
import Select from "react-select";
import AsyncSelect from "react-select/async";
import axios from "axios";

type SelectOption = { label: string; value: string };

const options = [
  { value: "SI", label: "Slovenija" },
  { value: "HR", label: "Hrvaška" },
  { value: "RS", label: "Srbija" },
  { value: "IT", label: "Italija" },
  { value: "AT", label: "Avstrija" },
  { value: "DE", label: "Nemčija" },
];

interface IProps {
  onLocationChange(city: string, country: string): void;

  label: string;
  ariaLabel: string;
  ariaLabelCountry: string;
  placeholder: string;
  instanceId: string;
}

export const LocationInput: FunctionComponent<IProps> = ({
  onLocationChange,
  label,
  ariaLabel,
  ariaLabelCountry,
  placeholder,
  instanceId,
}) => {
  const [countryValue, setCountryVal] = useState("SI");
  const [cityValue, setCityVal] = useState("");

  const promiseOptions = (inputValue: string) => {
    const url = `${process.env.NEXT_PUBLIC_API}/geonames/lookup/?query=${inputValue}&country=${countryValue}&collapse=true`;

    return axios.get(url).then((response) => {
      const items = response.data.suggestions.map((item: string) => {
        return { value: item, label: item };
      });

      return items;
    });
  };

  useEffect(() => {
    onLocationChange(cityValue, countryValue);
  }, [countryValue, cityValue]);

  const handleCityChange = (selectedOption: any) => {
    if (selectedOption) {
      setCityVal(selectedOption.value);
    } else {
      setCityVal("");
    }
  };

  const handleCountryChange = (selectedOption: any) => {
    setCityVal("");
    return setCountryVal(selectedOption.value);
  };

  return (
    <div className="mb-2">
      <div className="w-3/5 md:w-3/4 inline-block">
        <AsyncSelect
          className="mr-2"
          defaultOptions
          key={countryValue}
          loadOptions={promiseOptions}
          isClearable={true}
          value={cityValue ? { value: cityValue, label: cityValue } : null}
          onChange={handleCityChange}
          placeholder={placeholder}
          aria-label={ariaLabel}
          instanceId={instanceId}
          inputId={instanceId}
        />
      </div>
      <div className="w-2/5 md:w-1/4 inline-block">
        <Select
          options={options}
          defaultValue={options[0]}
          onChange={handleCountryChange}
          aria-label={ariaLabelCountry}
          instanceId={`${instanceId}-date`}
          inputId={`${instanceId}-date`}
        />
      </div>
    </div>
  );
};
