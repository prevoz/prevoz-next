import { FunctionComponent } from "react";
import GoogleAuth from "./auth/GoogleAuth";
import AppleAuth from "./auth/AppleAuth";
import UsernameAuth from "./auth/UsernameAuth";

const ProfileLogin: FunctionComponent = () => (
  <div className="bg-white rounded p-6 md:p-16 items-center flex flex-col">
    <div className="flex-grow my-6">
      <h2 className="text-xl">Trenutno nisi prijavljen</h2>
    </div>

    <div className="grid grid-cols-1 gap-4 flex-grow">
      <div>
        <GoogleAuth />
      </div>
      <div>
        <AppleAuth />
      </div>
      <div>
        <UsernameAuth />
      </div>
      <div className="pt-6 text-sm text-center">
        Priporočamo <b>Prijavo z Google računom</b>.
      </div>

      <p className="mt-6 text-sm">
        S prijavo se strinjate s pogoji uporabe storitve
        <br /> Prevoz.org
      </p>
    </div>
  </div>
);

export default ProfileLogin;
