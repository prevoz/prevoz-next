import React, { FunctionComponent, useEffect, useState } from "react";
import Select from "react-select";
import dayjs from "dayjs";

interface IProps {
  onDateChange(date: string): void;

  instanceId: string;
}

const getAvailableDates = () => {
  let resp = [];
  for (let offset = 0; offset < 30; offset++) {
    let day = dayjs().add(offset, "day");
    resp.push({
      value: day.format("YYYY-MM-DD"),
      label: day.format("dddd, D.M."),
    });
  }

  return resp;
};

export const DateInput: FunctionComponent<IProps> = ({
  onDateChange,
  instanceId,
}) => {
  const [availableDatesVal] = useState(getAvailableDates);
  const [dateVal, setDateVal] = useState(availableDatesVal[0]);

  useEffect(() => {
    onDateChange(dateVal.value);
  }, [availableDatesVal]);

  return (
    <>
      <Select
        options={availableDatesVal}
        onChange={(selectedOption: any) => {
          onDateChange(selectedOption.value);
          return setDateVal(selectedOption);
        }}
        value={dateVal}
        instanceId={`${instanceId}-date`}
        inputId={`${instanceId}-date`}
      />
    </>
  );
};
