import * as React from "react";
import Link from "next/link";
import Head from "next/head";

import LogoSvg from "../assets/svg/logo.svg";
import IconAddSvg from "../assets/svg/icon-add.svg";
import IconSearchSvg from "../assets/svg/icon-search.svg";
import IconLogoSvg from "../assets/svg/icon-logo.svg";
import IconUserSvg from "../assets/svg/icon-user.svg";
import { useContext } from "react";
import { MachineContext } from "../contexts/machineContext";

type Props = {
  title?: string;
};

const Layout: React.FunctionComponent<Props> = ({
  children,
  title = "Kam greš danes?",
}) => {
  const [authState, send] = useContext(MachineContext);
  const isAuthenticated = authState.context.user.isAuthenticated;

  return (
    <div className="container mx-auto pt-10 px-4">
      <Head>
        <title>{title} @ Prevoz.org</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <div className="grid grid-cols-4 gap-4">
        <nav className="nav-section">
          <a className="hidden md:block" href="/">
            <LogoSvg />
            <h1 className="mt-2 font-black text-2xl hidden lg:block">
              Kam greš danes?
            </h1>
          </a>
          <ul
            role="navigation"
            aria-label="Meni"
            className="flex flex-row md:flex-col justify-around"
          >
            <li className="order-1">
              <Link href="/">
                <a className="nav-tab">
                  <span className="nav-tab__img">
                    <IconSearchSvg />
                  </span>
                  <span className="nav-tab__text">Najdi prevoz</span>
                </a>
              </Link>
            </li>
            <li className="order-2">
              <Link href="/">
                <a className="nav-tab">
                  <span className="nav-tab__img">
                    <IconLogoSvg />
                  </span>

                  <span className="nav-tab__text">Moji prevozi</span>
                </a>
              </Link>
            </li>
            <li className="nav-tab__cta order-3 md:order-5">
              <Link href="/">
                <a className="nav-tab nav-tab-add">
                  <span className="nav-tab__img text-grey-800 md:text-white fill-current">
                    <IconAddSvg />
                  </span>
                  <span className="nav-tab__text">Dodaj Prevoz</span>
                </a>
              </Link>
            </li>
            <li className="order-4">
              <Link href={isAuthenticated ? "/profile" : "/accounts/login"}>
                <a className="nav-tab">
                  <span className="nav-tab__img">
                    <IconUserSvg />
                  </span>
                  <span className="nav-tab__text">Profil</span>
                </a>
              </Link>
            </li>
          </ul>
        </nav>

        <main className="col-span-4 md:col-span-3">{children}</main>
      </div>
    </div>
  );
};

export default Layout;
