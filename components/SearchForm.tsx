import React, { FunctionComponent, useState } from "react";
import { encodeQueryParams, StringParam } from "use-query-params";
import { LocationInput } from "./LocationInput";
import { DateInput } from "./DateInput";
import { useRouter } from "next/router";
import { stringify } from "querystring";

interface IProps {}

interface IState {
  from: string;
  fromCountry: string;
  to: string;
  toCountry: string;
  date: string;
}

export const SearchForm: FunctionComponent = () => {
  const [fromCity, setFromCity] = useState("");
  const [fromCountry, setFromCountry] = useState("SI");
  const [toCity, setToCity] = useState("");
  const [toCountry, setToCountry] = useState("SI");
  const [date, setDate] = useState("");

  const router = useRouter();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();

    const encodedQuery = encodeQueryParams(
      {
        f: StringParam,
        fc: StringParam,
        t: StringParam,
        tc: StringParam,
        d: StringParam,
      },
      {
        f: fromCity,
        fc: fromCountry,
        t: toCity,
        tc: toCountry,
        d: date,
      }
    );

    const url = `/rides?${stringify(encodedQuery)}`;
    router.push(url);
  };

  return (
    <div className="rounded bg-sand-100 p-5 mt-6">
      <form onSubmit={handleSubmit}>
        <LocationInput
          ariaLabel="Kraj odhoda"
          ariaLabelCountry="Država odhoda"
          label="Od"
          placeholder="Kraj odhoda"
          onLocationChange={(city: string, country: string) => {
            setFromCity(city);
            setFromCountry(country);
          }}
          instanceId="fromInput"
        />
        <LocationInput
          ariaLabel="Kraj prihoda"
          ariaLabelCountry="Država prihoda"
          placeholder="Kraj prihoda"
          label="Do"
          onLocationChange={(city: string, country: string) => {
            setToCity(city);
            setToCountry(country);
          }}
          instanceId="toInput"
        />
        <DateInput
          onDateChange={(date: string) => {
            setDate(date);
          }}
          instanceId="date-picker"
        />
        <button
          className="button orange w-full font-bold mt-6"
          type="submit"
          data-testid="search-button"
        >
          Najdi prevoz
        </button>
      </form>
    </div>
  );
};
