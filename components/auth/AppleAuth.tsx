import AppleLogin from "react-apple-login";
import { FunctionComponent } from "react";
import GoogleIcon from "./GoogleIcon";
import { GoogleLogin } from "react-google-login";
import { IconApple } from "../../assets/svg/icon-apple.svg";

const AppleAuth: FunctionComponent = () => {
  return (
    <AppleLogin
      clientId={process.env.NEXT_PUBLIC_APPLE_ID}
      redirectURI={process.env.NEXT_PUBLIC_APPLE_REDIRECT}
      responseType={"code"}
      responseMode={"query"}
      scope={"email,name"}
      render={(renderProps) => (
        <button onClick={renderProps.onClick} className="auth__apple">
          <IconApple alt="Apple" className="apple__icon" />
          <span>Prijava z Apple računom</span>
        </button>
      )}
    />
  );
};

export default AppleAuth;
