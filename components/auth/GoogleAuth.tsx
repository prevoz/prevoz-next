import { FunctionComponent, useContext } from "react";
import { GoogleLogin } from "react-google-login";
import GoogleIcon from "./GoogleIcon";
import { MachineContext } from "../../contexts/machineContext";

const GoogleAuth: FunctionComponent = () => {
  const [authState, send] = useContext(MachineContext);

  const handleGoogleResponse = (response) => {
    fetch(process.env.NEXT_PUBLIC_API_SOCIAL, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      mode: "cors",
      body: JSON.stringify({
        provider: "google-oauth2",
        code: response.code,
        redirect_uri: "postmessage",
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        send({
          type: "FETCH",
          token: response.data.token,
        });
      });
  };
  return (
    <GoogleLogin
      clientId="135306072364-7da0phdg1n7ara11suvd0l8qqsmesvrk.apps.googleusercontent.com"
      onSuccess={handleGoogleResponse}
      onFailure={handleGoogleResponse}
      cookiePolicy={"single_host_origin"}
      responseType="code"
      render={(renderProps) => (
        <button
          onClick={renderProps.onClick}
          disabled={renderProps.disabled}
          className="auth__google"
        >
          <GoogleIcon /> Prijava z Google računom
        </button>
      )}
    />
  );
};

export default GoogleAuth;
