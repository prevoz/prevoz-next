import { FunctionComponent } from "react";
import IconLogoSvg from "../../assets/svg/icon-logo.svg";
import Link from "next/link";

const UsernameAuth: FunctionComponent<{}> = ({}) => {
  return (
    <Link href="/accounts/login">
      <a className="auth__username">
        <IconLogoSvg className="username__icon" /> Prijava z uporabniškim imenom
      </a>
    </Link>
  );
};

export default UsernameAuth;
