import { FunctionComponent, PropsWithChildren } from "react";
import { useRouter } from "next/router";
import useSWR from "swr";
import fetcher from "../../../lib/fetcher";
import { IRide } from "../../../interfaces/ride";
import Layout from "../../../components/Layout";
import dayjs from "dayjs";

const RideView = () => {
  const router = useRouter();
  const rideId = router.query["id"];

  const { data, error } = useSWR(
    rideId ? `${process.env.NEXT_PUBLIC_API}/carshare/${rideId}/` : null,
    fetcher
  );

  if (error) {
    return (
      <Layout>
        <h1 className="text-3xl mt-12">
          Prišlo je napake pri pripravi prevoza ... :(
        </h1>
      </Layout>
    );
  } else if (!data) {
    return (
      <Layout>
        <h1 className="text-3xl mt-12" data-testid="view-loading">
          Pripravljamo prevoz ...
        </h1>
      </Layout>
    );
  } else if (data.detail) {
    return (
      <Layout>
        <h1 className="text-3xl mt-12">Prevoza nismo našli :(</h1>
      </Layout>
    );
  } else
    return (
      <Layout>
        <RideDisplay ride={data} />
      </Layout>
    );
};

const RideDisplay: FunctionComponent<{ ride: IRide }> = ({ ride }) => {
  return (
    <div className="bg-white rounded p-4">
      <div className="grid grid-cols-3">
        <LabeledItem label="Od">{ride.from}</LabeledItem>
        <LabeledItem label="Do">{ride.to}</LabeledItem>
        <LabeledItem label="Zaznamek">❤︎</LabeledItem>

        <LabeledItem label="Datum">
          {dayjs(ride.date_iso8601).format("dddd, D. MMMM")}
        </LabeledItem>
        <LabeledItem label="Ura">{ride.time}</LabeledItem>
        <LabeledItem label="Strošek" data-testid="view-price">
          {ride.price}
        </LabeledItem>

        <LabeledItem
          label="Voznik/ca"
          extraClass="border-none my-1 py-1"
          data-testid="view-author"
        >
          {ride.author ? ride.author : "-"}
        </LabeledItem>
        <LabeledItem
          label="Zavarovanje"
          extraClass="col-span-2 border-none my-1 py-1"
        >
          {ride.insured === "true" ? "Da" : "Ne"}
        </LabeledItem>
        <LabeledItem label="Avto" extraClass="col-span-3">
          {ride.car_info}
        </LabeledItem>
      </div>
    </div>
  );
};

const LabeledItem: FunctionComponent<PropsWithChildren<{
  label: string;
  extraClass?: string;
}>> = ({ label, extraClass, children, ...props }) => {
  return (
    <div className={`${extraClass} text-lg py-3 my-3 border-b`} {...props}>
      <div className="text-sm text-grey-300">{label}</div>
      {children}
    </div>
  );
};

export default RideView;
