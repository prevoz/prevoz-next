import { FunctionComponent } from "react";
import Layout from "../components/Layout";
import { SearchForm } from "../components/SearchForm";

const HomePage: FunctionComponent = () => (
  <Layout title="Hitreje domov">
    <h1 className="text-3xl font-black">Najdi prevoz</h1>
    <SearchForm />
  </Layout>
);

export default HomePage;
