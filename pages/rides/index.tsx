import { FunctionComponent } from "react";
import dayjs from "dayjs";
import { useRouter } from "next/router";

import { encodeQueryParams, StringParam } from "serialize-query-params";
import useSWR from "swr";
import { groupBy, sortBy } from "lodash";
import Link from "next/link";
import axios from "axios";

import Layout from "../../components/Layout";
import { IRide } from "../../interfaces/ride";
import { stringify } from "querystring";

import IconLogoSvg from "../../assets/svg/icon-logo.svg";

const cityMatch = /(.*[^\)])\((.*?)\)/;

function appendDistrict(ride: IRide): IRide {
  ["to", "from"].forEach((direction) => {
    if (ride[direction].includes("(")) {
      let matches = cityMatch.exec(ride[direction]);
      if (matches && matches.length === 3) {
        let city, district, _;
        [_, city, district] = matches;
        ride[direction] = city.trim();
        ride[`${direction}District`] = district;
      }
    }
  });

  return ride;
}

const RidesPage: FunctionComponent = () => {
  const router = useRouter();

  let searchParams = {
    fromCity: router.query["f"]?.toString(),
    fromCountry: router.query["fc"]?.toString(),
    toCity: router.query["t"]?.toString(),
    toCountry: router.query["tc"]?.toString(),
    date: router.query["d"]?.toString(),
  };

  const encodedQuery = encodeQueryParams(
    {
      f: StringParam,
      fc: StringParam,
      t: StringParam,
      tc: StringParam,
      d: StringParam,
      exact: StringParam,
    },
    {
      f: searchParams.fromCity || "",
      fc: searchParams.fromCountry || "",
      t: searchParams.toCity || "",
      tc: searchParams.toCountry || "",
      d: searchParams.date || "",
      exact: "false",
    }
  );

  const url = `${process.env.NEXT_PUBLIC_API}/search/shares/?${stringify(
    encodedQuery
  )}`;
  const { data, error } = useSWR(url, async (dataFetcher) => {
    let { data } = await axios.get(url);

    data.carshare_list.forEach((ride: IRide) => {
      return appendDistrict(ride);
    });
    let groupedRides = groupBy(data.carshare_list, (item) => {
      return `${item.from} - ${item.to}`;
    });

    /* Sort results so that exact search group is first, then the rest by alphabet */
    const sortKey = `${searchParams.fromCity} - ${searchParams.toCity}`;
    let rides = [];
    if (groupedRides[sortKey]) {
      rides.push({
        name: sortKey,
        items: sortBy(groupedRides[sortKey], ["date_iso8601"]),
      });
      delete groupedRides[sortKey];
    }
    Object.keys(groupedRides)
      .sort()
      .forEach((key) => {
        rides.push({
          name: key,
          items: sortBy(groupedRides[key], ["date_iso8601"]),
        });
      });

    return rides;
  });

  return (
    <Layout title="Rezultati iskanja">
      {error && <div>Prišlo je do napake ...</div>}
      {!data ? (
        <div data-testid="result-loading">Pripravljamo rezultate ...</div>
      ) : (
        <div className="max-w-2xl pb-20" data-testid="result-table">
          <h1 className="font-bold text-3xl">Najdi Prevoz</h1>
          <SearchResultsHeader date={searchParams.date} />
          {data.map(({ name, items }) => (
            <table key={name} className="mb-10 w-full rounded overflow-hidden">
              <ResultsHeader name={name} items={items} />
              <tbody>
                {items.map((ride: IRide) => (
                  <tr key={ride.id}>
                    <ResultItem ride={ride} />
                  </tr>
                ))}
              </tbody>
            </table>
          ))}
        </div>
      )}
    </Layout>
  );
};

const ResultsHeader: FunctionComponent<{ name: string; items: IRide[] }> = ({
  name,
  items,
}) => {
  const ride = items[0];

  return (
    <thead className="bg-sand-100" data-testid="result-header">
      <tr>
        <th className="text-left text-lg text-grey-800 font-black px-6 py-3">
          {ride.from} <span className="text-grey-300 mx-1">&gt;</span> {ride.to}
        </th>
        <th className="text-right font-normal text-sm px-6">
          {items.length} <IconLogoSvg className="inline h-14px -mt-1" />
        </th>
      </tr>
    </thead>
  );
};

const ResultItem: FunctionComponent<{ ride: IRide }> = ({ ride }) => (
  <td
    colSpan={2}
    className="bg-white b-color-sand-300 border-b hover:bg-sand-300 cursor-pointer"
    data-testid="result-item"
  >
    <Link
      href={{
        pathname: `/prevoz/view/${ride.id}/`,
      }}
    >
      <div className="flex px-8 py-3">
        <div className="flex-grow">
          <div className="font-bold leading-relaxed">
            {ride.time.replace("ob ", "")}
          </div>
          <div className="text-grey-300 text-sm">
            {ride.author ? ride.author : "-"}
            {ride.fromDistrict ? ` | ${ride.from} (${ride.fromDistrict})` : ""}
            {ride.toDistrict ? ` | ${ride.to} (${ride.toDistrict})` : ""}
          </div>
        </div>
        <div className="flex-shrink flex items-center font-bold">
          <span>{ride.price ? `${ride.price} €` : "-"}</span>
        </div>
      </div>
    </Link>
  </td>
);

const SearchResultsHeader: FunctionComponent<{ date: string | null }> = ({
  date,
}) => {
  if (date) {
    let formattedDate = dayjs(date).format("dddd, D. MMMM YYYY");
    formattedDate =
      formattedDate.charAt(0).toUpperCase() + formattedDate.slice(1);
    return <h2 className="font-bold text-2xl my-6">{formattedDate}</h2>;
  }

  return <></>;
};

export default RidesPage;
