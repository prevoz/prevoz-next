import { FunctionComponent, useContext, useEffect } from "react";
import Layout from "../../components/Layout";
import ProfileLogin from "../../components/ProfileLogin";
import { MachineContext } from "../../contexts/machineContext";
import { ProtectRoute } from "../../contexts/authContext";
import Router from "next/router";

export function useUser({ redirectTo = false, redirectIfFound = false } = {}) {
  const [authState, send] = useContext(MachineContext);
  const user = authState.context.user;

  useEffect(() => {
    if (!redirectTo || !user) return;

    if (
      // If redirectTo is set, redirect if the user was not found.
      (redirectTo && !redirectIfFound && !user?.isAuthenticated) ||
      // If redirectIfFound is also set, redirect if the user was found
      (redirectIfFound && user?.isAuthenticated)
    ) {
      Router.push("/accounts/login");
    }
  }, [user, redirectIfFound, redirectTo]);

  return { user };
}

const ProfilePage: FunctionComponent = () => {
  const [authState, send] = useContext(MachineContext);
  const { user } = useUser({ redirectTo: true });

  const logOut = (e) => {
    send("LOGOUT");
  };

  if (!user || user.isAuthenticated === false) {
    return (
      <Layout>
        <p>loading...</p>
      </Layout>
    );
  }

  return (
    <Layout title="Profil">
      {typeof window !== "undefined" && user.isAuthenticated ? (
        <div className="bg-white rounded p-6 md:p-16 items-center flex flex-col">
          <div className="flex-grow my-6">
            <h2 className="text-xl">
              Trenutno si prijavljen kot {user.username}{" "}
            </h2>
            <div className="my-6">
              <button className="button orange" onClick={logOut}>
                Odjavi me
              </button>
            </div>
          </div>
        </div>
      ) : (
        <ProfileLogin />
      )}
    </Layout>
  );
};

export default ProfilePage;
