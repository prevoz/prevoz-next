import "../styles/style.scss";

import { AppProps } from "next/app";
import createPersistedState from "use-persisted-state";

import { useEffect } from "react";

import dayjs from "dayjs";
import "dayjs/locale/sl";
import * as React from "react";
import { useMachine } from "@xstate/react";
import authMachine from "../lib/authMachine";
import { MachineContext } from "../contexts/machineContext";

dayjs.locale("sl");

const useAppState = createPersistedState("app");

function App({ Component, pageProps }: AppProps) {
  const [appState, setAppState] = useAppState(null);
  const [authState, send, service] = useMachine(authMachine, {
    state: appState || authMachine.initialState,
  });
  const machine = [authState, send, service];

  useEffect(() => {
    setAppState(authState);
  }, [authState]);

  return (
    <MachineContext.Provider value={machine}>
      <Component {...pageProps} />
    </MachineContext.Provider>
  );
}

export default App;
