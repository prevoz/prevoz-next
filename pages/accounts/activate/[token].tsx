import { useRouter } from "next/router";
import { FunctionComponent, useContext, useEffect, useState } from "react";
import Layout from "../../../components/Layout";
import Link from "next/link";
import axios, { AxiosError } from "axios";
import { MachineContext } from "../../../contexts/machineContext";

const RegistrationActivationView: FunctionComponent = () => {
  const router = useRouter();
  const tokenId = router.query["token"];
  const [hasError, setError] = useState(false);
  const [authState, send] = useContext(MachineContext);

  useEffect(() => {
    if (tokenId) {
      axios
        .get(`${process.env.NEXT_PUBLIC_API}/auth/activate/${tokenId}/`)
        .then((response) => {
          if (response.data.status === "success") {
            send({
              type: "FETCH",
              token: response.data.token,
            });
          } else {
            setError(true);
          }
        });
    }
  }, [tokenId]);

  return (
    <Layout>
      <h1 className="text-3xl font-black">Aktivacija računa</h1>
      <div className="bg-white rounded p-6 md:p-8 items-center flex flex-col mt-6">
        {hasError ? (
          <div data-testid="activation-error" className="flex-col items-center">
            <p>Napačna aktivacijska koda ali pa je račun že aktiviran.</p>
          </div>
        ) : (
          <div
            data-testid="activation-success"
            className="flex-col items-center"
          >
            <p>Uspešno ste aktivirali vaš račun.</p>
            <div className="grid grid-cols-2 gap-4 text-center mt-8">
              <Link href="/prevoz/add">
                <a className="button orange">Dodajte prevoz</a>
              </Link>
              <Link href="/">
                <a className="button orange">Poiščite prevoz</a>
              </Link>
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
};

export default RegistrationActivationView;
