import { FunctionComponent, useContext } from "react";
import Layout from "../../components/Layout";
import { useForm } from "react-hook-form";
import axios, { AxiosError } from "axios";
import { useRouter } from "next/router";
import Link from "next/link";
import { MachineContext } from "../../contexts/machineContext";

type Inputs = {
  username: string;
  password: string;
  nonFieldErrors: null;
};

const LOGIN_API = `${process.env.NEXT_PUBLIC_API}/auth/login/`;

const LoginView: FunctionComponent = () => {
  const router = useRouter();
  const [authState, send] = useContext(MachineContext);

  const {
    register,
    handleSubmit,
    errors,
    setError,
    clearErrors,
    reset,
  } = useForm<Inputs>({ mode: "onChange" });
  const onSubmit = async (data) => {
    let creds = window.btoa(`${data.username}:${data.password}`);
    clearErrors();

    await axios
      .post(
        LOGIN_API,
        {},
        {
          headers: {
            Authorization: `Basic ${creds}`,
          },
        }
      )
      .then(
        (response) => {
          send({
            type: "FETCH",
            token: response.data.token,
          });

          router.push("/profile");
        },
        (error: AxiosError) => {
          setError("nonFieldErrors", {
            type: "custom",
            message: error.response?.data?.detail || "Napaka pri prijavi",
          });
        }
      );
  };

  return (
    <Layout>
      <h1 className="text-3xl font-black">Prijava</h1>
      <div className="bg-white rounded p-6 md:p-8 items-center flex flex-col mt-6">
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="flex flex-col justify-between gap-4 w-full max-w-lg"
        >
          {errors.nonFieldErrors && (
            <span data-testid="auth-nonform-errors">
              {errors.nonFieldErrors.message}
            </span>
          )}
          <input
            name="username"
            defaultValue=""
            ref={register({ required: true })}
            placeholder="Uporabniško ime"
            className="auth__input"
            aria-invalid={errors.username ? "true" : "false"}
            onChange={() => clearErrors()}
            data-testid="auth-username"
            autoComplete="username"
          />
          {errors.username && <span role="alert">Polje je obvezno</span>}

          <input
            name="password"
            type="password"
            defaultValue=""
            ref={register({ required: true })}
            className="auth__input"
            placeholder="Geslo"
            aria-invalid={errors.password ? "true" : "false"}
            onChange={() => clearErrors()}
            data-testid="auth-password"
            autoComplete="current-password"
          />
          {errors.password && <span role="alert">Polje je obvezno</span>}

          <input
            className="button orange"
            type="submit"
            value="Prijava"
            data-testid="auth-submit"
          />
        </form>

        <p className="mt-6 text-sm">
          S prijavo se strinjate s pogoji uporabe storitve Prevoz.org
        </p>

        <div className="mt-6">
          <Link href="/accounts/register">
            <a className="button gray">Ustavi nov račun</a>
          </Link>
        </div>
      </div>
    </Layout>
  );
};

export default LoginView;
