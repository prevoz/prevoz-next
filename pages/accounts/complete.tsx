import { FunctionComponent, useContext, useEffect } from "react";
import Layout from "../../components/Layout";
import { useRouter } from "next/router";
import { MachineContext } from "../../contexts/machineContext";

const AccountsCompletePage: FunctionComponent = () => {
  const router = useRouter();
  const [authState, send] = useContext(MachineContext);

  const code = router.query["code"]?.toString();
  const error = router.query["error"]?.toString();

  useEffect(() => {
    if (code) {
      fetch(process.env.NEXT_PUBLIC_API_SOCIAL, {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json",
        },
        mode: "cors",
        body: JSON.stringify({
          provider: "apple-id",
          code: code,
          redirect_uri: `${process.env.NEXT_PUBLIC_APPLE_REDIRECT}`,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          send({
            type: "FETCH",
            token: data.token,
          });
        });
    }
  }, [code]);
  return (
    <Layout>
      Code is: {code}
      {error && <p>There was an error: {error}</p>}
    </Layout>
  );
};

export default AccountsCompletePage;
