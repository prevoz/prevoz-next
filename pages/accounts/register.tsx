import { FunctionComponent, useState } from "react";
import Layout from "../../components/Layout";
import { useForm } from "react-hook-form";
import axios from "axios";
import { ErrorMessage } from "@hookform/error-message";
import Link from "next/link";

type Inputs = {
  username: string;
  password: string;
  email: string;
  nonFieldErrors: null;
};

const REGISTER_API = `${process.env.NEXT_PUBLIC_API}/auth/register/`;

const RegisterView: FunctionComponent = () => {
  const [showConfirmation, setShowConfirmation] = useState(false);

  const { register, handleSubmit, errors, setError, clearErrors } = useForm<
    Inputs
  >({ mode: "onChange", criteriaMode: "all" });
  const onSubmit = async (data) => {
    await axios
      .post(REGISTER_API, {
        username: data.username,
        email: data.email,
        password1: data.password,
        password2: data.password,
      })
      .then((response) => {
        let data = response.data;
        if (data.status === "created") {
          setShowConfirmation(true);
        } else {
          Object.entries(data.error).forEach(
            ([key, value]: [keyof Inputs, string[]]) => {
              setError(key, {
                types: { [key]: value.join(" ") },
              });
            }
          );
        }
      });
  };

  // noinspection JSUnusedLocalSymbols
  let customErrorMessages = ({ messages, message }) => {
    return (
      messages &&
      Object.entries(messages).map(([type, message]) => {
        return type === "nonGmail" ? (
          <p key={type} data-testid={`error-${type}`}>
            Za prijavo z Gmail računom, uporabite{" "}
            <Link href="/profile">
              <a className="text-underline text-orange">Google prijavo</a>
            </Link>
            {"."}
          </p>
        ) : (
          <p key={type} data-testid={`error-${type}`}>
            {message}
          </p>
        );
      })
    );
  };

  return (
    <Layout>
      <h1 className="text-3xl font-black">Registracija</h1>
      <div className="bg-white rounded p-6 md:p-8 items-center flex flex-col mt-6">
        {showConfirmation ? (
          <p data-testid="auth-success">
            Uspešno ste se registrirali. Na vnešeni naslov boste prejeli e-mail.
            Sledite povezavi v e-mailu za dokončanje procesa registracije.
          </p>
        ) : (
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col justify-between gap-4 w-full max-w-lg"
          >
            {errors.nonFieldErrors && (
              <span data-testid="auth-nonform-errors">
                {errors.nonFieldErrors.message}
              </span>
            )}
            <input
              name="username"
              defaultValue=""
              ref={register({ required: true })}
              placeholder="Uporabniško ime"
              className="auth__input"
              aria-invalid={errors.username ? "true" : "false"}
              onChange={() => clearErrors()}
              data-testid="auth-username"
              autoComplete="username"
            />
            <ErrorMessage
              errors={errors}
              name="username"
              render={customErrorMessages}
            />
            <input
              name="email"
              defaultValue=""
              ref={register({
                required: "Polje je obvezno",
                validate: {
                  nonGmail: (email) => !email.includes("gmail"),
                },
              })}
              placeholder="E-mail"
              className="auth__input"
              aria-invalid={errors.email ? "true" : "false"}
              onChange={() => clearErrors()}
              data-testid="auth-email"
              autoComplete="email"
            />
            <ErrorMessage
              errors={errors}
              name="email"
              render={customErrorMessages}
            />

            <input
              name="password"
              type="password"
              defaultValue=""
              ref={register({ required: "Polje je obvezno" })}
              className="auth__input"
              placeholder="Geslo"
              aria-invalid={errors.password ? "true" : "false"}
              onChange={() => clearErrors()}
              data-testid="auth-password"
              autoComplete="current-password"
            />
            <ErrorMessage
              errors={errors}
              name="password"
              render={customErrorMessages}
            />

            <input
              className="button orange"
              type="submit"
              value="Registriraj se"
              data-testid="auth-submit"
            />

            <p>S prijavo se strinjate s pogoji uporabe storitve Prevoz.org</p>
          </form>
        )}
      </div>
    </Layout>
  );
};

export default RegisterView;
