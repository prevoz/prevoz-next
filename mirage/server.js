/* istanbul ignore file */
import { Server, Model, Factory, RestSerializer, Response } from "miragejs";
import faker from "faker";

const IzolaVeljenje = {
  from: "Izola",
  from_country: "SI",
  to: "Velenje",
  to_country: "SI",
  time: "ob 22:00",
  price: 15,
};

export function makeServer({ environment = "test" } = {}) {
  let server = new Server({
    environment,

    serializers: {
      carshare: RestSerializer.extend({
        keyForCollection(modelName) {
          if (modelName === "carshare") {
            return "carshare_list";
          }

          return super.keyForCollection(modelName);
        },
      }),
    },

    models: {
      carshare: Model,
    },

    factories: {
      carshare: Factory.extend({
        author(i) {
          return faker.name.firstName();
        },
        ...IzolaVeljenje,
      }),
    },

    routes() {
      this.namespace = "/api";

      this.get("/search/shares/", (schema) => {
        return schema.carshares.all();
      });

      this.get("/carshare/:id/", (schema, request) => {
        const id = request.params.id;
        return new Response(200, {}, schema.carshares.find(id).attrs);
      });

      this.post("/auth/register/", (_, request) => {
        let body = JSON.parse(request.requestBody);
        if (body.username === "user1") {
          return new Response(
            200,
            {},
            { status: "created", username: "user1" }
          );
        } else {
          return new Response(
            200,
            {},
            {
              status: "error",
              error: {
                email: ["E-naslov je že v uporabi, prosimo vnesite drugega."],
                username: ["Uporabnik s tem uporabniškim imenom že obstaja."],
              },
              non_field_errors: [],
            }
          );
        }
      });

      this.get(
        "/auth/activate/81020acefc36d456a46dcd47d3f239de032a4b1c/",
        (_, request) => {
          return new Response(
            200,
            {},
            {
              expiry: "2020-11-27T11:21:22.531997",
              status: "success",
              token:
                "7dee96adc4df69889693efbc3cabaff892a52becec071c57ad53c33b878cab96",
            }
          );
        }
      );

      this.get("/auth/activate/bad-token/", (_, request) => {
        return new Response(200, {}, { status: "error" });
      });

      this.post("/auth/login/", (_, request) => {
        // user1:pass123
        if (
          request.requestHeaders.Authorization === "Basic dXNlcjE6cGFzczEyMw=="
        ) {
          return new Response(
            200,
            {},
            {
              expiry: "2020-11-22T18:33:10.211228",
              token:
                "307a8482541bb8a6d1e4b2cb58aac27fdaf5e8e440a40f639447715f434ec6a0",
            }
          );
        } else {
          return new Response(
            401,
            {},
            { detail: "Napačno uporabniško ime ali geslo." }
          );
        }
      });
    },
  });

  return server;
}
