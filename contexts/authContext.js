import { createContext, useContext } from "react";
import { MachineContext } from "./machineContext";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  const [authState, send] = useContext(MachineContext);

  const user = authState.context.user;

  return (
    <AuthContext.Provider value={{ isAuthenticated: user.isAuthenticated }}>
      {children}
    </AuthContext.Provider>
  );
};
export const useAuth = () => useContext(AuthContext);

export const ProtectRoute = ({ children }) => {
  const { isAuthenticated } = useAuth();
  if (!isAuthenticated && window.location.pathname !== "/login") {
    return <p>Loading ...</p>;
  }
  return children;
};
