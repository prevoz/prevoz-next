export interface IRide {
  id: number;
  from: string;
  to: string;
  fromDistrict: string | null;
  toDistrict: string | null;
  time: string;
  author: string;
  car_info: string;
  date_iso8601: string;
  price: number | null;
  insured: string;

  [key: string]: any;
}
