import React from "react";
import {
  render,
  screen,
  fireEvent,
  act,
  cleanup,
} from "@testing-library/react";

import { waitFor } from "@testing-library/dom";
import { makeServer } from "../mirage/server";
import { useRouter } from "next/router";
import App from "../pages/_app";
import RegisterView from "../pages/accounts/register";
import RegistrationActivationView from "../pages/accounts/activate/[token]";

afterEach(cleanup);

describe("Username activation token", () => {
  const mockRouter = {
    asPath: "/accounts/token",
    push: jest.fn(),
    query: {},
  };

  beforeAll(() => {
    (useRouter as jest.Mock).mockReturnValue(mockRouter);
  });

  afterAll(() => {
    (useRouter as jest.Mock).mockReset();
  });

  let server;
  beforeEach(() => {
    server = makeServer();
  });

  afterEach(() => {
    server.shutdown();
  });

  test("renders activation error", async () => {
    mockRouter.asPath = "/accounts/token/bad-token/";
    mockRouter.query["token"] = "bad-token";
    render(
      <App
        pageProps={{}}
        Component={RegistrationActivationView}
        // @ts-ignore
        router={mockRouter}
      />
    );
    await waitFor(() => {
      expect(screen.getByTestId("activation-error")).toHaveTextContent(
        "Napačna aktivacijska koda ali pa je račun že aktiviran."
      );
    });
  });

  test("renders activation sucess", async () => {
    mockRouter.asPath =
      "/accounts/token/81020acefc36d456a46dcd47d3f239de032a4b1c/";
    mockRouter.query["token"] = "81020acefc36d456a46dcd47d3f239de032a4b1c";
    render(
      <App
        pageProps={{}}
        Component={RegistrationActivationView}
        // @ts-ignore
        router={mockRouter}
      />
    );
    await waitFor(() => {
      expect(screen.getByTestId("activation-success").textContent).toMatch(
        /Uspešno ste aktivirali vaš račun/
      );
    });
  });
});
