import React from "react";
import { cleanup, render, screen } from "@testing-library/react";
import { useRouter } from "next/router";
import { waitFor } from "@testing-library/dom";
import { makeServer } from "../mirage/server";

import RidesPage from "../pages/rides";
import App from "../pages/_app";
import LoginView from "../pages/accounts/login";

afterEach(cleanup);

describe("Search results", () => {
  const mockRouter = {
    asPath: "/",
    push: jest.fn(),
    query: {},
  };

  beforeAll(() => {
    (useRouter as jest.Mock).mockReturnValue(mockRouter);
  });

  afterAll(() => {
    (useRouter as jest.Mock).mockReset();
  });

  let server;
  beforeEach(() => {
    server = makeServer();
  });

  afterEach(() => {
    server.shutdown();
  });

  test("Search results page renders", async () => {
    server.createList("carshare", 5);

    mockRouter.asPath = "/rides/?f=&fc=SI&t=&tc=SI&d=";
    mockRouter.query = {
      query: {
        f: "",
        fc: "",
        t: "",
        tc: "",
        d: "",
      },
    };

    // @ts-ignore
    render(<App pageProps={{}} Component={RidesPage} router={mockRouter} />);
    await waitFor(() => {
      expect(screen.getByTestId("result-loading")).toHaveTextContent(
        "Pripravljamo rezultate ..."
      );
    });

    await waitFor(() => {
      const table = screen.getAllByTestId("result-header")[0];
      expect(table).toHaveTextContent("Izola > Velenje");
    });
  });
});
