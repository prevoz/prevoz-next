import React from "react";
import {
  render,
  screen,
  fireEvent,
  act,
  cleanup,
} from "@testing-library/react";
import LoginView from "../pages/accounts/login";
import { waitFor } from "@testing-library/dom";
import { makeServer } from "../mirage/server";
import { useRouter } from "next/router";
import App from "../pages/_app";

afterEach(cleanup);

describe("Username login form", () => {
  const mockRouter = {
    asPath: "/accounts/login",
    push: jest.fn(),
  };

  beforeAll(() => {
    (useRouter as jest.Mock).mockReturnValue(mockRouter);
  });

  afterAll(() => {
    (useRouter as jest.Mock).mockReset();
  });

  let server;
  beforeEach(() => {
    server = makeServer();
  });

  afterEach(() => {
    server.shutdown();
  });

  test("renders empty login form", async () => {
    // @ts-ignore
    render(<App pageProps={{}} Component={LoginView} router={mockRouter} />);
    await waitFor(() => {
      expect(screen.getByTestId("auth-username")).toHaveAttribute(
        "placeholder",
        "Uporabniško ime"
      );
      expect(screen.getByTestId("auth-password")).toHaveAttribute(
        "placeholder",
        "Geslo"
      );
    });
  });

  test("submits correct login form", async () => {
    // @ts-ignore
    render(<App pageProps={{}} Component={LoginView} router={mockRouter} />);
    await waitFor(() => {
      expect(screen.getByTestId("auth-username")).toHaveAttribute(
        "placeholder",
        "Uporabniško ime"
      );
    });

    let username = screen.getByTestId("auth-username");
    let password = screen.getByTestId("auth-password");
    await act(async () => {
      fireEvent.input(username, { target: { value: "user1" } });
      fireEvent.input(password, { target: { value: "pass123" } });
      fireEvent.submit(screen.getByTestId("auth-submit"));
    });

    await waitFor(() =>
      expect(mockRouter.push).toHaveBeenCalledWith("/profile")
    );
  });

  test("submits wrong user/pass", async () => {
    // @ts-ignore
    render(<App pageProps={{}} Component={LoginView} router={mockRouter} />);

    let username = screen.getByTestId("auth-username");
    let password = screen.getByTestId("auth-password");
    await act(async () => {
      fireEvent.input(username, { target: { value: "wronguser" } });
      fireEvent.input(password, { target: { value: "wrongpass" } });
      fireEvent.submit(screen.getByTestId("auth-submit"));
    });

    await waitFor(() => {
      expect(screen.getByTestId("auth-nonform-errors")).toHaveTextContent(
        "Napačno uporabniško ime ali geslo."
      );
    });
  });
});
