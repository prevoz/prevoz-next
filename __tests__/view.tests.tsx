import React from "react";
import { cleanup, render, screen } from "@testing-library/react";
import { useRouter } from "next/router";
import { waitFor } from "@testing-library/dom";
import { makeServer } from "../mirage/server";

import RideView from "../pages/prevoz/view/[id]";
import App from "../pages/_app";
import Index from "../pages";

describe("Basic detail view test", () => {
  const mockRouter = {
    asPath: "",
    query: {},
  };

  beforeAll(() => {
    (useRouter as jest.Mock).mockReturnValue(mockRouter);
  });

  afterAll(() => {
    (useRouter as jest.Mock).mockReset();
  });

  let server;
  beforeEach(() => {
    server = makeServer({ environment: "test" });
  });

  afterEach(() => {
    server.shutdown();
  });

  test("View displays basic information", async () => {
    mockRouter.asPath = "prevoz/view/5/";
    mockRouter.query["id"] = "5";

    const cs = server.create("carshare", { id: 5 });

    // @ts-ignore
    render(<App pageProps={{}} Component={RideView} router={mockRouter} />);

    await waitFor(() => {
      expect(screen.getByTestId("view-loading")).toHaveTextContent(
        "Pripravljamo prevoz ..."
      );
    });

    await waitFor(() =>
      expect(screen.getByTestId("view-author")).toHaveTextContent(
        "Voznik/ca" + cs["author"]
      )
    );

    await waitFor(() =>
      expect(screen.getByTestId("view-price")).toHaveTextContent(
        "Strošek" + cs["price"]
      )
    );
  });
});
