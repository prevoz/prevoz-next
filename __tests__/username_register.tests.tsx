import React from "react";
import {
  render,
  screen,
  fireEvent,
  act,
  cleanup,
} from "@testing-library/react";

import { waitFor } from "@testing-library/dom";
import { makeServer } from "../mirage/server";
import { useRouter } from "next/router";
import App from "../pages/_app";
import RegisterView from "../pages/accounts/register";

afterEach(cleanup);

describe("Username registration form", () => {
  const mockRouter = {
    asPath: "/accounts/register",
    push: jest.fn(),
  };

  beforeAll(() => {
    (useRouter as jest.Mock).mockReturnValue(mockRouter);
  });

  afterAll(() => {
    (useRouter as jest.Mock).mockReset();
  });

  let server;
  beforeEach(() => {
    server = makeServer();
  });

  afterEach(() => {
    server.shutdown();
  });

  test("renders empty registration form", async () => {
    // @ts-ignore
    render(<App pageProps={{}} Component={RegisterView} router={mockRouter} />);
    await waitFor(() => {
      expect(screen.getByTestId("auth-username")).toHaveAttribute(
        "placeholder",
        "Uporabniško ime"
      );
      expect(screen.getByTestId("auth-email")).toHaveAttribute(
        "placeholder",
        "E-mail"
      );
      expect(screen.getByTestId("auth-password")).toHaveAttribute(
        "placeholder",
        "Geslo"
      );
    });
  });

  test("submit correct login form", async () => {
    // @ts-ignore
    render(<App pageProps={{}} Component={RegisterView} router={mockRouter} />);
    await waitFor(() => {
      expect(screen.getByTestId("auth-username")).toHaveAttribute(
        "placeholder",
        "Uporabniško ime"
      );
    });

    await act(async () => {
      fireEvent.input(screen.getByTestId("auth-username"), {
        target: { value: "user1" },
      });
      fireEvent.input(screen.getByTestId("auth-password"), {
        target: { value: "pass123" },
      });
      fireEvent.input(screen.getByTestId("auth-email"), {
        target: { value: "user1@example.com" },
      });
      fireEvent.submit(screen.getByTestId("auth-submit"));
    });

    await waitFor(() => {
      expect(screen.getByTestId("auth-success")).not.toBeEmptyDOMElement();
    });
  });

  test("errors in registration form", async () => {
    // @ts-ignore
    render(<App pageProps={{}} Component={RegisterView} router={mockRouter} />);

    await act(async () => {
      fireEvent.input(screen.getByTestId("auth-username"), {
        target: { value: "error1" },
      });
      fireEvent.input(screen.getByTestId("auth-password"), {
        target: { value: "pass123" },
      });
      fireEvent.input(screen.getByTestId("auth-email"), {
        target: { value: "error1@example.com" },
      });
      fireEvent.submit(screen.getByTestId("auth-submit"));
    });

    await waitFor(() => {
      expect(screen.getByTestId("error-username")).toHaveTextContent(
        "Uporabnik s tem uporabniškim imenom že obstaja."
      );
    });
  });

  test("check for gmail in registration form", async () => {
    // @ts-ignore
    render(<App pageProps={{}} Component={RegisterView} router={mockRouter} />);

    await act(async () => {
      fireEvent.input(screen.getByTestId("auth-username"), {
        target: { value: "error2" },
      });
      fireEvent.input(screen.getByTestId("auth-password"), {
        target: { value: "pass123" },
      });
      fireEvent.input(screen.getByTestId("auth-email"), {
        target: { value: "error2@gmail.com" },
      });
      fireEvent.submit(screen.getByTestId("auth-submit"));
    });

    await waitFor(() => {
      expect(screen.getByTestId("error-nonGmail")).toHaveTextContent(
        "Za prijavo z Gmail računom, uporabite Google prijavo."
      );
    });
  });
});
