import React from "react";
import { render } from "@testing-library/react";
import Index from "../pages/index";
import App from "../pages/_app";
import RidesPage from "../pages/rides";

describe("The most basic rendering test", () => {
  const mockRouter = {
    asPath: "",
    query: {},
  };

  test("renders logo tagline", () => {
    const { getByText } = render(
      // @ts-ignore
      <App pageProps={{}} Component={Index} router={mockRouter} />
    );
    const linkElement = getByText(/Kam greš danes/);
    expect(linkElement).toBeInTheDocument();
  });
});
