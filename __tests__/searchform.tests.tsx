import React from "react";
import { cleanup, render, screen } from "@testing-library/react";
import { useRouter } from "next/router";
import { fireEvent } from "@testing-library/dom";
import { makeServer } from "../mirage/server";

import dayjs from "dayjs";
import { SearchForm } from "../components/SearchForm";

afterEach(cleanup);

describe("Tests homepage's SearchForm", () => {
  const mockRouter = {
    asPath: "/",
    push: jest.fn(),
  };

  beforeAll(() => {
    (useRouter as jest.Mock).mockReturnValue(mockRouter);
  });

  afterAll(() => {
    (useRouter as jest.Mock).mockReset();
  });

  let server;
  beforeEach(() => {
    server = makeServer();
  });

  afterEach(() => {
    server.shutdown();
  });

  test("search button exits and works", () => {
    const { getByText } = render(<SearchForm />);
    expect(getByText(/Najdi prevoz/)).toBeInTheDocument();
  });

  test("check for default placeholders", () => {
    const { getByText } = render(<SearchForm />);
    expect(getByText(/Kraj odhoda/)).toBeInTheDocument();
    expect(getByText(/Kraj prihoda/)).toBeInTheDocument();
  });

  test("it's possible to submit search form", () => {
    render(<SearchForm />);
    fireEvent.click(screen.getByTestId("search-button"));

    const date = dayjs().format("YYYY-MM-DD");
    expect(mockRouter.push).toHaveBeenCalledWith(
      `/rides?f=&fc=SI&t=&tc=SI&d=${date}`
    );
  });
});
